import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.AudioClip;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.io.*;

public class Main extends Application {
    private static Client c = new Client();
    private static Stage stage;
    private static Scene scene2;
    private Scene scene, scene1;
    private AnchorPane loading, menu, game;
    @FXML
    private Label l1, l2, l3, l4, l5, l6, l7, l8;
    @FXML
    private ImageView i1, i2, i3, i4, i5;
    @FXML
    private Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11;
    @FXML
    public TextField t1, t2;

    AudioClip aC = new AudioClip(Main.class.getResource("res/123.mp3").toExternalForm());
    public boolean err = true;

    public void start(Stage stage) throws Exception{
        try {
            menu = (AnchorPane) FXMLLoader.load(Main.class.getResource("smth.fxml"));
            loading = (AnchorPane) FXMLLoader.load(Main.class.getResource("paw.fxml"));
            game = (AnchorPane) FXMLLoader.load(Main.class.getResource("game.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        scene2 = new Scene(game);
        scene1 = new Scene(menu);
        scene = new Scene(loading);
        this.stage = stage;
        stage.setY(225);
        stage.setX(430);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
        new Timeline(new KeyFrame(Duration.millis(1500), actionEvent -> {
            aC.setCycleCount(999999999);
            aC.play();
            stage.setScene(scene1);
            stage.setX(0);
            stage.setY(0);
        })).play();
    }

    public static Stage getStage(){
        return stage;
    }

    public static Scene getScene() {
        return scene2;
    }

    @FXML
    public void initialize() {

        //Переключатели
        l1.setOnMouseEntered(ae -> {
            i1.setVisible(false);
            i3.setVisible(false);
            i2.setVisible(true);
            i5.setVisible(false);
        });

        l1.setOnMouseExited(ae -> {
            i2.setVisible(false);
            i3.setVisible(false);
            i1.setVisible(true);
            i5.setVisible(false);
        });

        l2.setOnMouseEntered(ae -> {
            i1.setVisible(false);
            i2.setVisible(false);
            i3.setVisible(true);
            i5.setVisible(false);
        });

        l2.setOnMouseExited(ae -> {
            i3.setVisible(false);
            i2.setVisible(false);
            i1.setVisible(true);
            i5.setVisible(false);
        });

        l5.setOnMouseEntered(ae -> {
            i3.setVisible(false);
            i2.setVisible(false);
            i1.setVisible(false);
            i5.setVisible(true);
        });

        l5.setOnMouseExited(ae -> {
            i3.setVisible(false);
            i2.setVisible(false);
            i5.setVisible(false);
            i1.setVisible(true);
        });

        //Нажиматели
        l1.setOnMouseClicked(ae -> {
            l1.setVisible(false);
            l2.setVisible(false);
            l5.setVisible(false);
            l3.setVisible(true);
            b1.setVisible(true);
            b2.setVisible(true);
            b3.setVisible(true);
            i4.setVisible(true);
            b1.setOnMouseClicked(aE -> {
                l3.setVisible(false);
                b1.setVisible(false);
                b2.setVisible(false);
                b3.setVisible(false);
                l8.setVisible(true);
                b10.setVisible(true);
                b11.setVisible(true);
                t2.setVisible(true);
                b10.setOnMouseClicked(Ae -> {
                    Main.getStage().setScene(Main.getScene());
                    //Здесь через txt файл передаётся строка в другой класс, он просто по какой-то причине тоже не видел содержимое той переменной, но это уже другая проблема
                    try(FileWriter writer = new FileWriter("IP.txt", false)){
                        writer.write(t2.getText());
                        writer.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        c.readingIP();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    new Thread(new Client()).start();
                    new Thread(new Server()).start();
                    //192.168.1.67
                });
                b11.setOnMouseClicked(Ae -> {
                    l8.setVisible(false);
                    b10.setVisible(false);
                    b11.setVisible(false);
                    t2.setVisible(false);
                    i4.setVisible(false);
                    l1.setVisible(true);
                    l2.setVisible(true);
                    l5.setVisible(true);
                });
            });
            b2.setOnMouseClicked(aE -> {
                b1.setVisible(false);
                b2.setVisible(false);
                b3.setVisible(false);
                l3.setVisible(false);
                l4.setVisible(true);
                b4.setVisible(true);
                b5.setVisible(true);
                t1.setVisible(true);
                b4.setOnMouseClicked(Ae -> {

                    //192.168.1.67
                });
                b5.setOnMouseClicked(Ae -> {
                    i4.setVisible(false);
                    b4.setVisible(false);
                    b5.setVisible(false);
                    t1.setVisible(false);
                    l1.setVisible(true);
                    l2.setVisible(true);
                    l5.setVisible(true);
                    l4.setVisible(false);
                });
            });
            b3.setOnMouseClicked(aE -> {
                l3.setVisible(false);
                b1.setVisible(false);
                b2.setVisible(false);
                b3.setVisible(false);
                l1.setVisible(true);
                l2.setVisible(true);
                l5.setVisible(true);
                i4.setVisible(false);
            });
        });

        l2.setOnMouseClicked(ae -> {
            i4.setVisible(true);
            l1.setVisible(false);
            l2.setVisible(false);
            l5.setVisible(false);
            l7.setVisible(true);
            b8.setVisible(true);
            b9.setVisible(true);
            b8.setOnMouseClicked(aE -> {
                if(err == false) {
                    aC.play();
                    err = true;
                }
                i4.setVisible(false);
                l1.setVisible(true);
                l2.setVisible(true);
                l5.setVisible(true);
                l7.setVisible(false);
                b8.setVisible(false);
                b9.setVisible(false);
            });
            b9.setOnMouseClicked(aE -> {
                aC.stop();
                err = false;
                i4.setVisible(false);
                l1.setVisible(true);
                l2.setVisible(true);
                l5.setVisible(true);
                l7.setVisible(false);
                b8.setVisible(false);
                b9.setVisible(false);
            });
        });

        l5.setOnMouseClicked(ae -> {
            l1.setVisible(false);
            l2.setVisible(false);
            l5.setVisible(false);
            i4.setVisible(true);
            b7.setVisible(true);
            l6.setVisible(true);
            b6.setVisible(true);
            b6.setOnMouseClicked(aE -> Main.getStage().close());
            b7.setOnMouseClicked(aE -> {
                i4.setVisible(false);
                l6.setVisible(false);
                b6.setVisible(false);
                b7.setVisible(false);
                l1.setVisible(true);
                l2.setVisible(true);
                l5.setVisible(true);
            });
        });
    }

    public static void main(String args[]) {
        launch(args);
    }

}
