import javafx.fxml.Initializable;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Client implements Runnable, Initializable {
    private static Main m = new Main();
    static private Socket connection;
    static private ObjectInputStream input;
    static private ObjectOutputStream output;
    public volatile String IP;

    public void readingIP() throws FileNotFoundException {
    //Соответственно здесь производиться запись строки в переменную, и она происходит без проблем
        File file = new File("IP.txt");
        Scanner scanner = new Scanner(file);
        IP = scanner.nextLine();
        System.out.println(IP);
    }

    public void run() {
        try {
            while(true){
            //Но уже здесь переменная IP считается пустой, хотя её перезапись произошла раньше запуска потока
                connection = new Socket(InetAddress.getByName(IP), 3111);
                output = new ObjectOutputStream(connection.getOutputStream());
                input = new ObjectInputStream(connection.getInputStream());
                System.out.println(IP);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void sendData(Object obj){
        try {
            output.flush();
            output.writeObject(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}