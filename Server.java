import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable {
    static private ServerSocket server;
    static private Socket connection;
    static private ObjectInputStream input;
    static private ObjectOutputStream output;

    public void run() {
        try {
            server = new ServerSocket(3111, 4);
            while(true){
                connection = server.accept();
                output = new ObjectOutputStream(connection.getOutputStream());
                input = new ObjectInputStream(connection.getInputStream());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
